Feature('Login_test');
//usar hashtag para tratar id//
// add --grep '@sucesso' para rodar tags//
// para executar em headless, alterar a linha show para false em codecept.conf.js//
Scenario('Login com sucesso',  ({ I }) => {

  I.amOnPage('http://automationpratice.com.br')
  I.click('Login')
  I.waitForText('Login',  10)
   I.fillField('#user', 'jessicapereirabatista@gmail.com')
   I.fillField('#password', 'Batista@91')
   I.click('#btnLogin')
   I.waitForText('Login realizado' , 3)
})

Scenario('Tentando logar digitando apenas o email',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br')
    I.click('Login')
   I.waitForText('Login',  10)
   I.fillField('#user', 'jessicapereirabatista@gmail.com')
   I.fillField('#password', '')
   I.click('#btnLogin')
   I.waitForText('Senha inválida' , 3)
  }).tag('@sucesso')
  
  Scenario('Tentando logar sewm digitar o email e senha ',  ({ I }) => {

   I.amOnPage('http://automationpratice.com.br')
   I.click('Login')
   I.waitForText('Login',  10)
   I.fillField('#user', 'jessicapereirabatista@gmail.com')
   I.fillField('#password', '')
   I.click('#btnLogin')
   I.waitForText('Senha inválida' , 3)
  })